const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	entry: ['custom-event-polyfill', 'promise-polyfill/dist/polyfill.js', 'unfetch/polyfill.js', './src/index.js'],
	output: {
	  	path: path.resolve(__dirname, 'dist'),
	  	filename: 'senna-cognito-s3.js',
      sourceMapFilename: 'senna-cognito-s3.js.map',
	  	library: 'SC3',
	},
  module: {
      rules: [
          {
              test: /\.js$/,
              exclude: /node_modules/,
              use: {
                  loader: 'babel-loader',
                  options: {
                      presets: ['babel-preset-env'],
                      plugins: ['babel-plugin-transform-runtime'],
                  }
              }
          }
      ]
  },
  externals: {
	    Modal: {
	      root: 'Modal',
      },
      Collapse: {
	      root: 'Collapse',
      }
  },
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        extractComments: true,
        sourceMap: true,
      })
    ]
  }
};
