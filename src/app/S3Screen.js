import { HtmlScreen } from 'senna';
import S3 from 'aws-sdk/clients/s3';
import UA from 'metal-useragent';
import Uri from 'metal-uri';
import app from '../app';

class S3Screen extends HtmlScreen {

    constructor() {
        super();
        this.setCacheable(true);
        this.s3 = new S3({
            apiVersion: '2006-03-01',
            params: { Bucket: app.getBucket() }
        });
        app.register(this);
    }

    _getKey(path) {
      const uri = new Uri(path);
      const parts = uri.getPathname().split('/').filter(p => p.length > 0);
      if (!parts.length || !parts[parts.length - 1].match(/\.html$/)) {
        parts.push('index.html');
      }
      return parts.join('/');
    }

    _load(path) {
        const cache = this.getCache();
        if (cache !== undefined && cache !== null) {
            return Promise.resolve(cache);
        }

        return new Promise((resolve, reject) => {
            this.s3.getObject({
                Key: this._getKey(path),
            }, (err, data) => {
                if (err) {
                    if (err.code === 'AccessDenied') {
                      return app.login()
                        .then(() => this._load(path))
                        .then(resolve)
                        .catch(reject);
                    } else {
                      return reject(err);
                    }
                }

                const text = data.Body.toString('utf8');
                if (this.isCacheable()) {
                    this.addCache(text);
                }
                resolve(text);
            });
        });
    }

    load(path) {
        return this._load(path)
            .then(content => {
                this.allocateVirtualDocumentForContent(content);
                this.resolveTitleFromVirtualDocument();
                this.assertSameBodyIdInVirtualDocument();
                if (UA.isIe) {
                    this.makeTemporaryStylesHrefsUnique_();
                }
                return content;
            });
    }
}

export default S3Screen;
