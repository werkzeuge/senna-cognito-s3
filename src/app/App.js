import { dataAttributeHandler } from 'senna';
import AWS from 'aws-sdk/global';
import { CognitoUser, CognitoUserPool, AuthenticationDetails } from 'amazon-cognito-identity-js';
import Uri from 'metal-uri';
import LoginDialog from './LoginDialog';

export default class App {

  constructor() {
    this.credentialsType = null;
    this.credentialsData = {};
    this.poolData = {};
    this.userPool = null;
    this.user = null;
    this.bucket = window.location.host;
    this.region = '';
    this.logout = null;
    this.routes = [];
  }

  handle() {
    return new Promise(resolve =>
      document.addEventListener('DOMContentLoaded', () => {
        this._internalHandle();
        resolve(this);
      })
    );
  }

  _internalHandle() {
    this._load();
    this._loadBucket();
    this._loadRegion();
    this._loadCredentials();
    this._loadCognito();

    this._attach();
    this._validateSession();

    this._scrollToView = (event) => {
      const pageHeader = this._querySelector('.navbar');
      const collapseHeader = this._querySelector(`[data-target="#${event.target.id}"]`);
      const offset = collapseHeader.getBoundingClientRect().top -
        collapseHeader.offsetTop -
        pageHeader.offsetHeight - pageHeader.offsetTop;
      window.scrollBy({top: offset, left: 0, behavior: 'smooth'});
    };
    this._registerHandlers();
    this._registerScrollOnOpen();

    const uri = new Uri(window.location.href);
    this._openAnchor(uri);

    this.dialog = new LoginDialog(this);
  }

  navigate(path) {
    const uri = new Uri(path);
    return dataAttributeHandler.getApp().navigate(uri.getPathname() + uri.getSearch() + uri.getHash());
  }

  register(route) {
    if (route != null) {
      this.routes.push(route);
    }
  }

  _clearCache() {
    this.routes.forEach((route) => {
      route.clearCache();
    });
  }

  _initBootstrap() {
    const BSN = window.BSN;
    if (BSN) {
      BSN.initCallback(document.body);
    }
  }

  _registerScrollOnOpen() {
    const togglers = this._querySelectorAll('[data-toggle="collapse"]');
    togglers.map((toggler) => this._querySelector(toggler.dataset.target))
      .filter(collapse => !!collapse)
      .forEach((collapse) => {
        collapse.addEventListener('shown.bs.collapse', this._scrollToView);
      });
  }

  _callAnalytics(uri) {
    const ga = window.ga;
    if (ga) {
      console.log('calling analytics');
      ga('set', 'page', uri.getPathname());
      ga('send', 'pageview');
    }
  }

  _openAnchor(uri) {
    const hash = uri.getHash();
    if (hash) {
      const anchor = document.getElementById(hash.substr(1));
      if (anchor) {
        const toggleElement = anchor.querySelector('[data-toggle="collapse"]');
        if (toggleElement && toggleElement.Collapse) {
          toggleElement.Collapse.show();
        }
      }
    }
  }

  _registerHandlers() {
    dataAttributeHandler.getApp().on('endNavigate', (event) => {
      if (!event.error) {
        console.log(`navigated to ${event.path}`);
        const uri = new Uri(event.path);
        this._callAnalytics(uri);
        this._initBootstrap();
        this._registerScrollOnOpen();
        this._openAnchor(uri);
      }
    });
  }

  login() {
    return this.dialog.open()
      .then((credentials) => this.authenticate(credentials));
  }

  authenticate(credentials) {
    const authDetails = new AuthenticationDetails({ Username: credentials.username, Password: credentials.password });
    this.user = new CognitoUser({ Username: credentials.username, Pool: this.userPool });

    return new Promise((resolve, reject) => {
      this.user.authenticateUser(authDetails, {
        onSuccess: (result) => {
          console.log(`access token ${result.getIdToken().getJwtToken()}`);

          this.setToken(result.getIdToken().getJwtToken());
          this.refresh()
            .then(() => {
              console.log('Successful login!');
              this.dialog.close();
              resolve();
            })
            .catch(reject);
        },
        onFailure: (err) => {
          console.error(err);
          this.dialog.error();
          reject(err);
        },
      });
    });
  }

  refresh() {
    return new Promise((resolve, reject) => {
      AWS.config.credentials.refresh((error) => {
        if (error) {
          this._logout();
          reject(error);
        } else {
          this._login();
          resolve();
        }
      });
    });
  }

  refreshSession(refreshToken) {
    return new Promise((resolve, reject) => {
      this.user.refreshSession(refreshToken, (err, session) => {
        if(err) {
          return reject(err);
        }
        resolve(session);
      });
    })
  }

  getBucket() {
    return this.bucket;
  }

  setCredentials() {
    AWS.config.credentials = new this.credentialsType(this.credentialsData);
  }

  setToken(token) {
    AWS.config.credentials.params.Logins = AWS.config.credentials.params.Logins || {};
    AWS.config.credentials.params.Logins[`cognito-idp.${this.region}.amazonaws.com/${this.poolData.UserPoolId}`] = token;
  }

  _querySelector(selector) {
    return document.querySelector(selector);
  }

  _querySelectorAll(selector) {
    return Array.prototype.slice.call(document.querySelectorAll(selector));
  }

  _stringToObject(str) {
    if (!str) {
      return {};
    }
    return str.split(',').reduce((obj, pair) => {
      const [key, ...values] = pair.split(':');
      obj[key] = values.join(':');
      return obj;
    }, {});
  }

  _load() {
    this.logout = this._querySelector('[data-cognito-logout]');
  }

  _attach() {
    if (this.logout == null) {
      console.error('Logout not found');
      return;
    }

    this.logout.addEventListener('click', (e) => {
      e.preventDefault();
      this._logout();
    }, false);
  }

  _loadBucket() {
    const bucketSelector = 'link[rel="aws-s3-bucket"]';
    const link = this._querySelector(bucketSelector);
    if (link) {
      this.bucket = link.getAttribute('href');
      console.log(`Setting AWS S3 bucket to ${this.bucket}`);
    }
  }

  _loadRegion() {
    const regionSelector = 'link[rel="aws-region"]';
    const link = this._querySelector(regionSelector);
    if (link) {
      const region = link.getAttribute('href');
      console.log(`Setting AWS region to ${region}`);
      AWS.config.region = this.region = region;
    }
  }

  _loadCognito() {
    const cognitoSelector = 'link[rel="cognito"]';
    const link = this._querySelector(cognitoSelector);
    if (link) {
      this.poolData = this._stringToObject(link.getAttribute('href'));
      this.userPool = new CognitoUserPool(this.poolData);
      console.log(`Setting Cognito pool data to (${JSON.stringify(this.poolData)})`);
    }
  }

  _loadCredentials() {
    const identityPoolSelector = 'link[rel="aws-credentials"]';
    const link = this._querySelector(identityPoolSelector);
    if (link) {
      const type = link.getAttribute('type');
      this.credentialsType = AWS[type];
      this.credentialsData = this._stringToObject(link.getAttribute('href'));
      console.log(`Setting AWS credentials to ${type}(${JSON.stringify(this.credentialsData)})`);
      this.setCredentials();
    }
  }

  _login() {
    if (this.logout) {
      this.logout.classList.remove('d-none');
    }
  }

  _logout() {
    if (this.user != null) {
      this.user.signOut();
      this.setCredentials();
      this._clearCache();
    }
    if (this.logout) {
      this.logout.classList.add('d-none');
    }
    window.location.href = '/';
  }

  _validateSession() {
    if (!this.userPool) {
      return;
    }

    this.user = this.userPool.getCurrentUser();
    if (this.user != null) {
      this.user.getSession((err, session) => {
        if (err) {
          console.log(err);
          return;
        }
        console.log(`Session validity: ${session.isValid()}`);

        let promise = Promise.resolve(session);
        if (!session.isValid()) {
          promise = promise.then((s) => this.refreshSession(s.getRefreshToken()));
        }
        promise.then((s) => {
          this.setToken(s.getIdToken().getJwtToken());
          this.refresh();
        });
      });
    }
  }
}
