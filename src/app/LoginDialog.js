export default class LoginDialog {

  constructor() {
    this.element = null;
    this.modal = null;
    this.submit = null;
    this.username = null;
    this.password = null;

    this._load();
    this._attach();
  }

  _load() {
    this.element = document.querySelector('[data-cognito-modal]');
    this.modal = new Modal(this.element, {});
    this.submit = this.element.querySelector('[type="submit"]');
    this.username = this.element.querySelector('input[type="text"]');
    this.password = this.element.querySelector('input[type="password"]');
  }

  _attach() {
    if (this.submit == null || this.element == null) {
      console.error('Submit and/or element not found.');
      return;
    }

    this.submit.addEventListener('click', (e) => {
      e.preventDefault();
      if (this.resolve) {
        this._enable();
        this._disable();
        this.resolve({ username: this.username.value, password: this.password.value });
      }
    }, false);
    this.element.addEventListener('hidden.bs.modal', () => {
      this._clear();
    }, false);
  }

  _enable() {
    this.username.removeAttribute('disabled');
    this.password.removeAttribute('disabled');
    this.submit.removeAttribute('disabled');
  }

  _disable() {
    this.username.setAttribute('disabled', 'disabled');
    this.password.setAttribute('disabled', 'disabled');
    this.submit.setAttribute('disabled', 'disabled');
  }

  _valid() {
    this.username.classList.remove('is-invalid');
    this.password.classList.remove('is-invalid');
  }

  _invalid() {
    this.username.classList.add('is-invalid');
    this.password.classList.add('is-invalid');
  }

  _clear() {
    this.username.value = '';
    this.password.value = '';
    this._valid();
    this._enable();
    if (this.reject) {
      this.reject({});
    }
    this.resolve = null;
    this.reject = null;
  }

  open() {
    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
      this.modal.show();
    });
  }

  error() {
    this._invalid();
    this._enable();
  }

  close() {
    this.modal.hide();
  }
}
