import { HtmlScreen } from 'senna';
import S3Screen from './app/S3Screen';
import app from './app';

window.HtmlScreen = HtmlScreen;
window.S3Screen = S3Screen;

module.exports = app.handle();
